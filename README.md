# Traefik reverse proxy configuration for (Docker) development environment

## Setup

Choose a domain name, like traefik.localhost, and add it to your /etc/hosts (for Linux) :

```
127.0.0.1   traefik.localhost
```

Clone this repository and execute the following commands :

```
# Create a network that will be shared by reverse proxied projects
docker network create dev-network

docker-compose up -d
```

For HTTP reverse proxy : open http://traefik.localhost and you should now see the Traefik dashboard

For HTTPS reverse proxy, open https://traefik.localhost, add an exception by the auto-generated Traefik self signed certificate and you should see the Traefik dashboard.

## Add another project to the reverse proxy

Modify your host file like above to route a domain, here demo-app.localhost, to localhost

You will add to blocks to the docker-compose file :

At the bottom of the file :

```
networks:
  default:
    external:
        name: dev-network
```

This configuration will tell Docker to create the services in the same docker network, so Traefik can act as a reverse proxy.

Inside a service definition like nodejs app, along with volumes or ports, add and adapt the following :

```
    labels:
      - traefik.enable=true
      - traefik.frontend.rule=Host:demo-app.localhost
      - traefik.port=8080
```

This 3 rules will :
- Expose the service outside Traefik
- Redirect the request for demo-app.localhost domain request to this container
- The port 8080 is reverse proxied to HTTP and HTTPS
